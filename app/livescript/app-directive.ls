#= require app-service.js

angular.module \app.directive, <[ app.service ]>

#
# Executes the expression when the bottom of element is shown in screen.
#
# Usage: <div vu-end-reached="populate(items)"
#             ng-repeat="item in items"
#         ></div>
#
# If vu-end-offset is provided, the expression is executed when the bottom of elem
# is near the bottom of window for <vu-end-offset> pixels.
#
.directive 'vuEndReached', <[
       $window
]> ++ ($window) ->

  # Returned config obj
  link: (scope, elem, attrs) ->
    # If no callbacks assigned, there is no need initailizing this directive.
    return unless attrs.vuEndReached

    # Cached DOM rect value. Calculate them right away.
    #
    rect = windowHeight = null
    do !function update-cache
      rect := elem[0].getBoundingClientRect!
      windowHeight := $window.innerHeight

    # Debounced scroll event handling using requestAnimationFrame
    # http://www.html5rocks.com/en/tutorials/speed/animations/
    #
    is-requesting = false

    # Do the callback and reset the is-requesting flag
    !function do-request
      scope.$eval attrs.vuEndReached,
        $unsubscribe: unsubscribe-scroll
        $rect: rect
        $windowHeight: windowHeight

      is-requesting := false

    # Offset threshold
    const threshold = +attrs.vuEndOffset || 0

    !function scrollHandler
      # Update rect from DOM
      updateCache!

      # Cancel requesting if is currently requesting or the end is not reached yet
      #
      # rect.bottom: dist(screen top, element bottom)
      #
      return if is-requesting || rect.bottom > windowHeight + threshold

      $window.requestAnimationFrame do-request
      is-requesting := true

    !function unsubscribe-scroll
      angular.element($window).off 'scroll', scroll-handler

    angular.element($window).on 'scroll', scroll-handler

    scope.$on '$destroy', unsubscribe-scroll