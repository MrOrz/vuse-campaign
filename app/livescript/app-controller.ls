#= require vuse.js
#= require app-constant.js
#= require app-router.js

app = angular.module 'app.controller', <[app.constants app.router vuse]>

app.controller \AppCtrl, <[
         $state
]> ++ !(@$state) ->
  console.log "AppCtrl loaded"

app.controller \TextTabCtrl, <[
        CHANNEL_ID  $timeout  Channel
]> ++ !(CHANNEL_ID, $timeout, Channel) ->

  # Digest loop intiated by $resource, no need for manual triggering
  #
  # $timeout !-> console.log('Digest loop initializing')
  #   , 100

  # Population control
  @cards = []
  is-populating = false
  page = 0
  @populate = (unsubscribe) !~>
    return if is-populating
    is-populating := true

    page += 1

    # Get data using VUSE api.
    data <~! Channel.submitted-eggs channelId: CHANNEL_ID, page: page
    console.log 'Eggs', data
    if data.length is 0
      unsubscribe!
    else
      @cards = @cards ++ data

    is-populating := false

app.controller \PostCtrl, <[
        ResolvedData
]> ++ !(ResolvedData) ->
  @data = ResolvedData.egg


app.controller \DelayedDigestCtrl, <[
        $timeout
]> ++ !($timeout) ->
  # Triggers the digest loop after 100ms ......
  $timeout -> console.log('Digest loop initializing')
  , 100