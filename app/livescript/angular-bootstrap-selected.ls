#
# Bring all files & modules required by specific angular-ui-bootstrap component
# together
#
#= require angular-template.js
#= require angular-bootstrap/src/transition/transition.js
#= require ui-bootstrap-modal/index.js

angular.module 'ui-bootstrap-modal', <[angular.template ui.bootstrap.modal]>