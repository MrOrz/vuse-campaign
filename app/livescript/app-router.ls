#= require angular-ui-router/release/angular-ui-router.min.js
#= require ui-router-extras/release/ct-ui-router-extras.min.js
#= require underscore/underscore.js
#= require angular-bootstrap-selected.js
#= require vuse.js

angular.module 'app.router', <[
  ui.router
  ct.ui.router.extras
  vuse
  ui-bootstrap-modal
]>
.config <[
        $stateProvider  $urlRouterProvider
]> ++ !($stateProvider, $urlRouterProvider) ->
  $urlRouterProvider.otherwise '/'

  #
  # http://plnkr.co/edit/joqCreywFtnUCFMzVzBF?p=preview
  #

  $stateProvider
  .state \post, do
    url: '/post/:id'

    # When traversed to this state, immediately transfer to
    # one of the proxy states.
    proxy: # Custom config processed in $stateChangeStart
      internal: 'post.modal'
      external: 'post.full'

    # Empty the view of 'post' state
    # https://github.com/christopherthielen/ui-router-extras/issues/11#issuecomment-48071905
    views: {}

    resolve:
      ResolvedData: <[Egg $stateParams]> ++ (Egg, $params) ->
        Egg.get({eggId: $params.id}).$promise

  .state \post.modal, do

    is-modal: true # Custom config processed in $stateChangeStart
    views:
      \modal@ :
        templateUrl: 'public/templates/detail.html'
        controller: 'PostCtrl as Post'

  .state \post.full, do
    views:
      \tabs@ :
        templateUrl: 'public/templates/detail.html'
        controller: 'PostCtrl as Post'

  .state \tabs, do
    sticky: true
    deepStateRedirect: true
    abstract: true
    url: ''
    templateUrl: 'public/templates/tabs.html'

  .state \tabs.hatched do
    url: '/'
    templateUrl: 'public/templates/text-tab.html'
    controller: 'TextTabCtrl as TextTab'

.run <[
        $rootScope  $state  $modal
]> ++ !($rootScope, $state, $modal) ->

  state-behind-modal = {}
  modal-instance = null

  $rootScope.$on \$stateChangeStart, !(evt, to-state, to-params, from-state, from-params) ->

    #
    # Implementing "proxy": redirect to the state according to where it's from.
    #
    if to-state.proxy
      evt.prevent-default!

      if from-state.name is '' || from-state.name is to-state.proxy.external
        # Visiting directly via URL or from the external state,
        # redirect to external (full) state.
        $state.go to-state.proxy.external, to-params

      else
        # Visiting from another state, redirect to internal (modal) state
        $state.go to-state.proxy.internal, to-params

      return


    # Implementing "isModal":
    # perform the required action to transitions between "modal states" and "non-modal states".
    #

    if !from-state.is-modal && to-state.is-modal
      #
      # Non-modal state ---> modal state
      #

      state-behind-modal =
        state: from-state
        params: from-params

      console.log 'stateBehindModal', state-behind-modal

      # Open up modal
      modal-instance = $modal.open do
        template: '<div ui-view="modal"><h1 class="text-center">Loading</h1></div>'

      modal-instance.result.finally ->
        # Reset instance to mark that the modal has been dismissed.
        modal-instance = null

        # Go to previous state
        $state.go state-behind-modal.state, state-behind-modal.params


    else if from-state.is-modal && !to-state.is-modal
      #
      # Modal state ---> non-modal state
      #

      # Directly return if the instance is already dismissed.
      return if !modal-instance

      # Dismiss the modal, triggering the reset of modal-instance
      modal-instance.dismiss!
