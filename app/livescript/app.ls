#= require angular/angular.min.js
#= require angular-deckgrid/angular-deckgrid.js

#= require app-router.js
#= require app-service.js
#= require app-controller.js
#= require app-directive.js
#= require app-template.js

app = angular.module 'app', <[
  akoenig.deckgrid
  app.router
  app.controller
  app.template
  app.directive
]>

# Make Rails happy
app.config <[
       $httpProvider
]> ++ ($httpProvider) ->
  $httpProvider.defaults.headers.common["Content-Type"] = ""
  $httpProvider.defaults.headers.common["X-Requested-With"] = \XMLHttpRequest
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRF-Token'
