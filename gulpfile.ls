require! {
  connect
  gulp
  'gulp-compass'
  'gulp-livereload'
  'gulp-filter'
  'gulp-uglify'
  'gulp-jade'
  'gulp-angular-templatecache'
  child_process.exec
  child_process.execFile
}

const PATHS =
  JS: <[
      app/livescript/*.ls
      app/livescript/*.ls.ejs
      vendor/bower_components/**/*.js
      !vendor/bower_components/angular-bootstrap/misc/*
      tmp/javascripts/*.js
    ]>
  HTML:
    TEMPLATES: <[app/jade/templates/*.jade]>
    ROOT: <[app/jade/*.jade]>
    BOOTSTRAP: <[vendor/bower_components/angular-bootstrap/template/modal/*.html]>
  CSS: <[
      app/sass/*.sass
    ]>


Sprocket = require 'sprocket'
Sprocket.viewLocals.isProduction = process.env.NODE_ENV is \production

environment = new Sprocket.Environment()

# Override Sprocket sass engine to support compass
#
# environment.registerEngine '.sass', !(env, src, dest) ->
#   src.pipe gulp-compass do
#     config_file: 'config/compass.rb'
#     css: 'public/stylesheets'
#     sass: 'app/sass'
#   .on \error, (err) -> console.error err
#   .pipe dest
# , mime_type: 'text/css'

# Override Sprocket js engine because jshint throws error for no reason
#
environment.register-engine '.js', !(env, src, dest) ->
  filter = gulp-filter <[**/*.js !**/*.min.js]>
  stream = src.pipe filter
  stream .= pipe gulp-uglify! if env.is-production
  stream.pipe filter.restore!
        .pipe dest
, mime_type: 'application/javascript'

# Override Sprocket html engine to bypass gulp-minify-htm
#
environment.registerEngine '.html', !(env, src, dest) ->
  src.pipe dest
, mime_type: 'text/html'


gulp.task \default, <[server]>

# Jade tempalte compilation
#
gulp.task \html, <[js css]>, ->
  gulp.src PATHS.HTML.ROOT
    .pipe environment.create-htmls-stream!
    .pipe gulp.dest './'

# Campaign template cache.
# All templates are collected into module "app.template".
#
gulp.task \template, ->
  gulp.src PATHS.HTML.TEMPLATES
  .pipe gulp-jade doctype: \html
  .pipe gulp-angular-templatecache 'app-template.js', do
    module: 'app.template'
    root: 'public/templates/'
    standalone: true
  .pipe gulp.dest 'tmp/javascripts'

# angular-ui-bootstrap selected template cache.
# All selected templates are collected into module "angular.template".
#
gulp.task \bootstrap-template, ->
  gulp.src PATHS.HTML.BOOTSTRAP
  .pipe gulp-angular-templatecache 'angular-template.js', do
    module: 'angular.template'
    root: 'template/modal'
    standalone: true
  .pipe gulp.dest 'tmp/javascripts'

# Sass compilation using compass
#
gulp.task \compass, ->
  gulp.src PATHS.CSS
  .pipe gulp-compass do
    config_file: 'config/compass.rb'
    css: 'public/stylesheets'
    sass: 'app/sass'
  .on \error, (err) -> console.error err
  .pipe gulp.dest 'tmp/stylesheets/'

gulp.task \css, <[compass]> ->
  gulp.src 'tmp/stylesheets/*.css'
    .pipe environment.createStylesheetsStream()
    .pipe gulp.dest 'public/stylesheets/'

# Livescript compilation
#
gulp.task \js, <[bootstrap-template template]>, ->
  gulp.src PATHS.JS
  .pipe environment.createJavascriptsStream()
  .pipe gulp.dest 'public/javascripts/'

gulp.task \clean !(cb) ->
  exec 'rm -rf build'
  cb(null)

# Project building
#
gulp.task \build, <[clean]>, !(cb) ->
  buildProcess = execFile 'script/build.sh', cb
  buildProcess.stdout.pipe process.stdout
  buildProcess.stderr.pipe process.stderr

# # Push to github
# #
gulp.task \push, <[build]>,  !(cb) ->
  pushProcess = execFile 'script/push.sh', cb
  pushProcess.stdout.pipe process.stdout
  pushProcess.stderr.pipe process.stderr

# Spin up a localhost server, default port = 5000
#
gulp.task \server, <[html]>, ->
  console.log 'Starting livereload server...'

  gulp.watch PATHS.CSS ++ PATHS.JS ++ PATHS.HTML.TEMPLATES ++ PATHS.HTML.ROOT ++ PATHS.HTML.BOOTSTRAP, <[html]>

  gulp.watch <[index.html]>
      .on 'change', gulp-livereload.changed

  gulp-livereload.listen!

  console.log 'Starting connect server...'
  server = connect()
  port = process.env.PORT || 3000
  server.use connect.static('./') .listen port, ->
    console.log "Connect server starting at http://localhost:#port"



gulp.task \deploy, <[clean build push]>