#!/bin/bash
# Commits all files in build/ directory to repository's gh-pages

NODE_ENV=production gulp html
mkdir build

for i in `cat script/exports.txt`
do
cp -R $i build/
done
